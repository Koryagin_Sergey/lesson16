package com.sourceit.lesson16;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class ArticleRecyclerAdapter extends RecyclerView.Adapter<ArticleRecyclerAdapter.ViewHolder> {

    private List<Article> list;
    private Context context;
    private OnArticleClick onArticleClick;

    public ArticleRecyclerAdapter(List<Article> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_layout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    public void setOnArticleClick(OnArticleClick onArticleClick) {
        this.onArticleClick = onArticleClick;
    }
    public  void addToPosition (Article article, int position){
        list.add(position, article);
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    interface OnArticleClick {
        void onItemClick(int position);
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            text = (TextView) itemView.findViewById(R.id.text);
            itemView.findViewById(R.id.root).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onArticleClick.onItemClick(getAdapterPosition());
                }
            });
        }

        void bind(Article article) {
            title.setText(article.getTitle());
            text.setText(article.getText());
        }
    }


}
