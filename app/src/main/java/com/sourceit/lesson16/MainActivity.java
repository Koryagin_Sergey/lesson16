package com.sourceit.lesson16;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    RecyclerView list;
    ArticleRecyclerAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (RecyclerView) findViewById(R.id.list);
        adapter = new ArticleRecyclerAdapter(Generation.generateList(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);

        adapter.setOnArticleClick(new ArticleRecyclerAdapter.OnArticleClick() {
            @Override
            public void onItemClick(int position) {
//                Toast.makeText(MainActivity.this,article.getTitle(),
//                        Toast.LENGTH_SHORT).show();
                Article article = new Article("title" + position, "text");
                adapter.addToPosition(article,position);


            }
        });
    }
}
