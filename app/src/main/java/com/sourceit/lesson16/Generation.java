package com.sourceit.lesson16;


import java.util.ArrayList;
import java.util.List;

public class Generation {
    private Generation() {
    }

    public static Article[] generate() {
        Article[] articles = new Article[10];
        for (int i = 0; i < 10; i++) {
            articles[i] = new Article(
                    "Title" + i,
                    "Long long text " + i
            );
        }
        return articles;
    }

    public static List<Article> generateList() {
        List<Article> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(new Article("title " + i, "text " + i));
        }
        return list;
    }


}

